<?php
/*
 * @copyright 2019-2021 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 14.08.21 03:34:53
 */

declare(strict_types = 1);

use dicr\justin\Justin;

/**  */
const YII_ENV = 'dev';
/**  */
const YII_DEBUG = true;

require_once(dirname(__DIR__) . '/vendor/autoload.php');
require_once(dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php');

/** @noinspection PhpUnhandledExceptionInspection */
new yii\web\Application([
    'id' => 'test',
    'basePath' => dirname(__DIR__),
    'components' => [
        'cache' => [
            'class' => yii\caching\FileCache::class
        ],
        'log' => [
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning', 'info', 'trace']
                ]
            ]
        ],
    ],
    'modules' => [
        'justin' => [
            'class' => dicr\justin\JustinModule::class,
            'url' => Justin::TEST_URL,
            'login' => Justin::TEST_LOGIN,
            'passwd' => Justin::TEST_PASSWD
        ]
    ],
    'bootstrap' => ['log']
]);
